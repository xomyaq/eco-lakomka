var _ = require('lodash')
  , actionName = {
      fb: {
        partner_page: {
          title: 'Расскажи в Facebook',
          desc: 'Здесь подробное описание задания. Здесь подробное описание задания. Здесь подробное описание задания.',
          pic: 'fb-2'
        },
        like: {
          title: 'Вступи в группу Facebook',
          desc: 'Здесь подробное описание задания. Здесь подробное описание задания. Здесь подробное описание задания.',
          pic: 'fb'
        }
      },
      vk: {
        partner_page: {
          title: 'Расскажи в VK',
          desc: 'Здесь подробное описание задания. Здесь подробное описание задания. Здесь подробное описание задания.',
          pic: 'vk-2'
        },
        like: {
          title: 'Вступи в группу VK',
          desc: 'Здесь подробное описание задания. Здесь подробное описание задания. Здесь подробное описание задания.',
          pic: 'vk'
        }
      },
      tw: {
        partner_page: {
          title: 'Расскажи в Твиттер',
          desc: 'Здесь подробное описание задания. Здесь подробное описание задания. Здесь подробное описание задания.',
          pic: 'profile'
        },
        like: {
          title: 'Подпишись на Твиттер',
          desc: 'Здесь подробное описание задания. Здесь подробное описание задания. Здесь подробное описание задания.',
          pic: 'profile'
        }
      },
      ok: {
        partner_page: {
          title: 'Расскажи в ОК',
          desc: 'Здесь подробное описание задания. Здесь подробное описание задания. Здесь подробное описание задания.',
          pic: 'profile'
        },
        like: {
          title: 'Вступай в группу в ОК',
          desc: 'Здесь подробное описание задания. Здесь подробное описание задания. Здесь подробное описание задания.',
          pic: 'profile'
        }
      },
      gp: {
        partner_page: {
          title: 'Расскажи в Google+',
          desc: 'Здесь подробное описание задания. Здесь подробное описание задания. Здесь подробное описание задания.',
          pic: 'profile'
        },
        like: {
          title: 'Вступай в группу в Google+',
          desc: 'Здесь подробное описание задания. Здесь подробное описание задания. Здесь подробное описание задания.',
          pic: 'profile'
        }
      },
      emailBinding: {
        sample: {
          title: 'Оставьте ваш емейл',
          desc: 'Здесь подробное описание задания. Здесь подробное описание задания. Здесь подробное описание задания.',
          pic: 'profile'
        }
      },
      inviteFriend: {
        sample: {
          title: 'Пригласите друзей',
          desc: 'Здесь подробное описание задания. Здесь подробное описание задания. Здесь подробное описание задания.',
          pic: 'invite'
        }
      }
    };

angular.module('app').controller('ActionsController', [
  '$scope', 'sailplayService', 'actionsService',
  ($scope, sailplayService, actionsService) => {
    let updateActions = (res) => {
      actionsService.setActions(res.actions);

      $scope.$apply(() => {
        $scope.actionsList = _.map(res.actions, action => {
          let type = action.socialType ? action.socialType : action.type
            , data = actionName[type][action.action ? action.action : 'sample']
            , title = data.title
            , desc = data.desc
            , pic = data.pic;

          return { title, desc, pic, action }
        });
      });
    };

    $scope.actionsList = [];
    $scope.actionInfo = {};
    $scope.actionInfoVisible = false;

    $scope.actionPerform = action => {
      sailplayService.actionPerform(action).then(sailplayService.actionsList).then(updateActions);
    };

    $scope.showActionInfo = data => {
      if ($scope.actionInfo && $scope.actionInfo.title === data.title) {
        $scope.actionInfoVisible = !$scope.actionInfoVisible;
      } else {
        $scope.actionInfoVisible = true;
        $scope.actionInfo = data;
      }
    };

    sailplayService.actionsList().then(updateActions);
  }
]);
