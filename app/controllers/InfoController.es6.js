var _ = require('lodash');

angular.module('app').controller('InfoController', [
  '$scope', 'sailplayService', 'actionsService',
  ($scope, sailplayService, actionsService) => {
    $scope.userName = ' ';
    $scope.userPoints = 0;
    $scope.userStatus = {};
    $scope.userHistory = [];
    $scope.badgesList = [];
    $scope.showHistory = false;
    $scope.showStatus = false;

    $scope.showBadgeInfo = (badge) => {
      $scope.showHistory = false;

      if ($scope.userStatus && $scope.userStatus.name === badge.name) {
        $scope.showStatus = !$scope.showStatus;
      } else {
        $scope.showStatus = true;
        $scope.userStatus = badge;
      }
    };

    $scope.share = type => {
      let actions = actionsService.getActions()
        , action = _.find(actions, { socialType: type });

      sailplayService.actionPerform(action);
    };

    Promise.all([
      sailplayService.userInfo(),
      sailplayService.userHistory(),
      sailplayService.badgesList()
    ]).then((res) => {
      let [userInfo, userHistory, badgesList] = res;

      $scope.$apply(() => {
        $scope.userName = userInfo.user.name;
        $scope.userPoints = userInfo.user_points.total;
        $scope.currentStatus = userInfo.last_badge.name;
        $scope.userHistory = _.map(userHistory, entry => {
          let date = new Date(entry.action_date)
            , day = date.getDate()
            , month = date.getMonth() + 1;

          entry.date = `${ day < 10 ? '0' + day : day }.${ month < 10 ? '0' + month : month }.${ date.getFullYear() }`;
          entry.name = entry.name ? entry.name : 'Начисление баллов';
          entry.pic = entry.pic ? entry.pic : '//d3257v5wstjx8h.cloudfront.net/media/assets/assetfile/cd055ab57af7f13da33c44829bfcb700.png';

          return entry;
        });
        $scope.badgesList = badgesList.multilevel_badges[0];
      });
    });
  }
]);
