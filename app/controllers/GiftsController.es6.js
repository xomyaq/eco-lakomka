angular.module('app').controller('GiftsController', [
  '$scope', 'sailplayService',
  ($scope, sailplayService) => {
    $scope.gifts = [{
      title: 'Купон на 500 рублей'
    }, {
      title: 'Купон на 1000 рублей'
    }, {
      title: 'Купон на 2000 рублей'
    }];
    $scope.giftInfo = false;

    $scope.showGiftInfo = (data) => {
      $scope.giftInfo = true;
    };

    //sailplayService.giftsList().then((res) => {
    //  console.log(res);
    //});
  }
]);
