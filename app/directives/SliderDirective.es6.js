var sliderTemplate = require('../templates/SliderTemplate.html');

angular.module('app').directive('spSlider', () => {
  return {
    restrict: 'A',
    scope: {
      slides: '=slides',
      showInfo: '&slideSelect'
    },
    template: sliderTemplate,
    link(scope, element, attrs) {
      let viewportWidth = element.width()
        , slidesToShow = attrs.slidesToShow ? parseInt(attrs.slidesToShow, 10) : 3
        , slideWidth = viewportWidth / slidesToShow
        , track = element.find('.sp-slider__track')
        , farPos, trackWidth;

      scope.$watch('slides', (slides) => {
        trackWidth = slides.length * slideWidth;
        farPos = -viewportWidth * Math.floor(slides.length / slidesToShow)
        track.width(trackWidth);
      });

      scope.slideRight = () => {
        let pos = parseInt(track.css('left'), 10) + viewportWidth;

        if (pos <= 0) {
          track.css({ left: pos })
        } else {
          track.css({ left: farPos });
        }
      };

      scope.slideLeft = () => {
        let pos = parseInt(track.css('left'), 10) - viewportWidth;

        if (Math.abs(pos) < trackWidth) {
          track.css({ left: pos });
        } else {
          track.css({ left: 0 })
        }
      };
    }
  }
});
