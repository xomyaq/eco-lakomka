var _ = require('lodash')
  , statusTemplate = require('../templates/StatusTemplate.html');

angular.module('app').directive('spStatus', () => {
  return {
    restrict: 'A',
    scope: {
      badges: '=badges',
      showInfo: '&badgeSelect'
    },
    template: statusTemplate,
    link(scope, element, attrs) {
      scope.$watch('badges', badges => {
        let len = badges.length
          , currBadgeIdx = _.findLastIndex(badges, 'is_received');

        if (len > 3 && currBadgeIdx !== -1) {
          if (currBadgeIdx === len - 1) {
            scope.firstBadge = badges[currBadgeIdx - 2];
            scope.secondBadge = badges[currBadgeIdx - 1];
            scope.thirdBadge = badges[currBadgeIdx];
          } else if (currBadgeIdx === 0) {
            scope.firstBadge = badges[currBadgeIdx];
            scope.secondBadge = badges[currBadgeIdx + 1];
            scope.thirdBadge = badges[currBadgeIdx + 2];
          } else {
            scope.firstBadge = badges[currBadgeIdx - 1];
            scope.secondBadge = badges[currBadgeIdx];
            scope.thirdBadge = badges[currBadgeIdx + 1];
          }
        } else {
          scope.firstBadge = badges[0];
          scope.secondBadge = badges[1];
          scope.thirdBadge = badges[2];
        }
      });
    }
  }
});
