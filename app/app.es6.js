var appRoot = document.getElementById('sailplay-widget')
  , indexView = require('./views/IndexView.html')
  , partnerId = appRoot.getAttribute('data-partner-id')
  , authHash = appRoot.getAttribute('data-auth-hash');

// Angular custom scrollbar
//require('ng-scrollbar');
//require('ng-scrollbar/dist/ng-scrollbar.css');

var app = angular.module('app', []);

// Require styles
require('./styles/main.styl');

// Require modules
require('./directives/SliderDirective.es6');
require('./directives/StatusDirective.es6');
require('./services/ActionsService.es6');
require('./services/SailplayService.es6');
require('./controllers/InfoController.es6');
require('./controllers/ActionsController.es6');
require('./controllers/GiftsController.es6');

app.run(['sailplayService', (sailplayService) => {
  sailplayService.init(partnerId).then(() => sailplayService.login(authHash));
}]);

appRoot.innerHTML = indexView;
angular.bootstrap(appRoot, ['app']);
