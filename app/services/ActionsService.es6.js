angular.module('app').factory('actionsService', () => {
  let actions = [];

  return {
    setActions(actionsList) {
      actions = actionsList;
    },

    getActions() {
      return actions;
    }
  }
});
