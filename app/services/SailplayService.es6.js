var SP = require('SAILPLAY');

angular.module('app').factory('sailplayService', () => {
  var partnerId
    , authHash
    , instance
    , user;

  return {
    init(id) {
      partnerId = id;

      return instance ? instance : instance = new Promise((resolve, reject) => {
        SP.send('init', { partner_id: id });

        SP.on('init.success', resolve);
        SP.on('init.error', reject);
      });
    },

    login(hash) {
      authHash = hash;

      return user ? user : user = new Promise((resolve, reject) => {
        SP.send('login', hash);

        SP.on('login.success', resolve);
        SP.on('login.error', reject);
      });
    },

    userInfo() {
      return instance.then(user).then(() =>
        new Promise((resolve, reject) => {
          SP.send('load.user.info');

          SP.on('load.user.info.success', resolve);
          SP.on('load.user.info.error', reject);
        })
      )
    },

    userHistory() {
      return instance.then(user).then(() =>
        new Promise((resolve, reject) => {
          SP.send('load.user.history');

          SP.on('load.user.history.success', resolve);
          SP.on('load.user.history.error', reject);
        })
      )
    },

    giftsList() {
      return instance.then(user).then(() =>
        new Promise((resolve, reject) => {
          SP.send('load.gifts.list');

          SP.on('load.gifts.list.success', resolve);
          SP.on('load.gifts.list.error', reject);
        })
      )
    },

    badgesList() {
      return instance.then(user).then(() =>
        new Promise((resolve, reject) => {
          SP.send('load.badges.list');

          SP.on('load.badges.list.success', resolve);
          SP.on('load.badges.list.error', reject);
        })
      )
    },

    actionsList() {
      return instance.then(user).then(() =>
        new Promise((resolve, reject) => {
          SP.send('load.actions.list');

          SP.on('load.actions.list.success', resolve);
          SP.on('load.actions.list.error', reject);
        })
      )
    },

    actionPerform(action) {
      return instance.then(user).then(() => new Promise((resolve, reject) => {
        SP.send('actions.perform', action);

        SP.on('actions.perform.complete', resolve);
        SP.on('actions.perform.auth.error', reject);
      }));
    }
  }
});
