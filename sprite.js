var nsg = require('node-sprite-generator');

nsg({
  src: ['./app/images/icons/*.png'],
  spritePath: './app/images/sprite.png',
  stylesheetPath: './app/styles/sprite.styl',
  //stylesheet: 'stylus',
  //stylesheetOptions: {
  //  pixelRatio: 2
  //}
}, function (err) {
  if (err) {
    return console.error(err);
  }

  console.log('sprite generated');
});
