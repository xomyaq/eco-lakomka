'use strict';

var path = require('path')
  , webpack = require('webpack')
  , ngMinPlugin = require('ngmin-webpack-plugin')
  , nib = require('nib');

var appDir = path.resolve(__dirname, './app')
  , distDir = path.resolve(__dirname, './dist')
  , bowerDir = path.resolve(__dirname, './bower_components');

module.exports = {
  devtool: 'eval',
  cache: true,
  debug: false,

  entry: [
    path.join(appDir, 'app.es6.js')
  ],

  output: {
    path: distDir,
    filename: 'app.js',
    chunkFilename: '[id].app.js'
  },

  externals: {
    'SAILPLAY': 'SAILPLAY',
    'jquery': 'jQuery'
  },

  module: {
    loaders: [{
      test: /\.es6\.js$/,
      loader: 'babel'
    }, {
      test: /\.css$/,
      loader: 'style!css'
    }, {
      test: /\.styl$/,
      loader: 'style!css!stylus'
    }, {
      test: /\.html$/,
      loader: 'raw'
    }, {
      test: /\.(jpe?g|png|gif|svg)$/i,
      loader: 'url?prefix=dist/'
    }],

    noParse: [
      path.join(bowerDir, '/lodash'),
      path.join(bowerDir, '/angular'),
      path.join(bowerDir, '/ng-scrollbar')
    ]
  },

  stylus: {
    use: [nib()]
  },

  resolve: {
    alias: {
      bower: bowerDir,
      //angular: path.resolve(bowerDir, 'angular/angular.min.js'),
      lodash: path.resolve(bowerDir, 'lodash/lodash.min.js')
    },
    extensions: ['', '.js', '.styl', '.css'],
    root: bowerDir
  },

  plugins: [
    new webpack.ResolverPlugin([
      new webpack.ResolverPlugin.DirectoryDescriptionFilePlugin("bower.json", ["main"])
    ]),

    new webpack.ContextReplacementPlugin(/.*$/, /a^/),

    new webpack.ProvidePlugin({
      'angular': 'exports?window.angular!bower/angular/angular.min.js'
    }),

    new ngMinPlugin(),

    new webpack.optimize.UglifyJsPlugin()
  ]
};
